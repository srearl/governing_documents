# BYLAWS OF LA CUESTA ASSOCIATION

## ARTICLE I
### NAME AND LOCATION

The name of the corporation is LA CUESTA ASSOCIATION, hereinafter referred to as the "Association". The principal office of the corporation shall be located at Scottsdale, Arizona; but meetings of members and directors may be held at such places within the State of Arizona, County of Maricopa, as may be designated by the Board of Directors.

## ARTICLE II
### DEFINITIONS

Section 1. "Association" shall mean and refer to LA CUESTA ASSOCIATION, its successors and assigns.

Section 2. "Properties" shall mean and refer to that certain real property described in the Declaration of Covenants, Conditions and Restrictions, and such additions thereto as may hereafter be brought within the jurisdiction of the Association.

Section 3. "Common Area" shall mean all real property owned by the Association for the common use and enjoyment of the Owners.

Section 4. "Lot" shall mean and refer to any plot of land shown upon any recorded subdivision map of the properties with the exception of the Common Area. 

Section 5. "Owner" shall mean and refer to the record owner, whether one or more persons or entities, of the fee simple title to any Lot which is a part of the properties, including contract sellers, but excluding those having such interest merely as security for the performance of an obligation.

Section 6. "Declarant" shall mean and refer to Arizona Title Insurance and Trust Company, an Arizona corporation, its successors and assigns if such successors or assigns should acquire more than one undeveloped Lot from the Declarant for the purpose of development.

Section 7. "Developer" shall mean and refer to Continental Homes, Inc., an Ohio corporation, or any successor to all or substantially all of the interest of said corporation in the development.

Section 8. "Declaration" shall mean and refer to the Declaration of Covenants, Conditions and Restrictions applicable to the properties recorded in the books and records of the County of Maricopa, Arizona, in Book of BLANK Maps, pages BLANK.

Section 9. "Member" shall mean and refer to those persons entitled to membership as provided in the Declaration.

## ARTICLE IIT
### MEETING OF MEMBERS

Section 1. Annual Meetings. The first annual meeting of the members shall be held within one year from the date of incorporation of the Association; and each subsequent regular annual meeting of the members shall be held on the same day of the same month of each year thereafter, at the hour of 7:30 o'clock P.M. If the day for the annual meeting of the members is a legal holiday, the meeting will be held at the same hour on the first day following which is not a legal holiday.

Section 2. Special Meetings. Special meetings of the Members may be called at any time by the President or by the Board of Directors or upon written request of the members who are entitled to vote one-fourth (1/4) of all of the votes of the Class A membership.

Section 3. Notice of Meetings. Written notice of each meeting of the members shall be given by, or at the direction of, the Secretary or person authorized to call the meeting, by mailing a copy of such notice, postage prepaid, at least thirty (30) days before such meeting to each member entitled to vote thereat, addressed to the member's address last appearing on the books of the Association, or supplied by such member to the Association for the purpose of notice. Such notice shall specify the place, day and hour of the meeting and, in the case of a special meeting, the purpose of the meeting.

Section 4. Quorum. The presence at the meeting of members entitled to cast, or of proxies entitled to cast, one-tenth (1/10) of the votes of each class of membership shall constitute a quorum for any action except as otherwise provided in the Articles of Incorporation, the Declaration or these Bylaws. If, however, such quorum shall not be present or represented at any meeting, the members entitled to vote thereat shall have the power to adjourn the meeting from time to time, without notice other than announcement at the meeting, until a quorum as aforesaid shall be present or be represented.

Section 5. Proxies. At all meetings of members, each member may vote in person or by proxy. All proxies shall be in writing and filed with the secretary. Every proxy shall be revocable and shall automatically cease upon conveyance by the member of his Lot.

## ARTICLE IV
### BOARD OF DIRECTORS - SELECTION - TERM OF OFFICE

Section 1. Number. The affairs of this Association shall be managed by a Board of not less than three (3), nor more than twenty-five (25) Directors, who need not be members of the Association.

Section 2. Term of Office. At the first annual meeting, the members shall elect three (3) Directors for a term of one (1) year, three (3) Directors for a term of two (2) years and three (3) Directors for a term of three (3) years; and, at each annual meeting thereafter, the members shall elect three (3) Directors for a term of three (3) years.

Section 3. Removal. Any Director may be removed from the Board, with or without cause, by a majority vote of the members of the Association. In the event of death, resignation or removal of a Director, his successor shall be selected by the remaining members of the Board and shall serve for the unexpired term of his predecessor.

Section 4. Compensation. No Director shall receive compensation for any service he may render to the Association. However, any Director may be reimbursed for his actual expenses incurred in the performance of his duties.

Section 5. Action Taken Without a Meeting. The Directors shall have the right to take any action in the absence of a meeting which they could take at a meeting by obtaining the written approval of all the Directors. Any action so approved shall have the same effect as though taken at a meeting of the Directors.

## ARTICLE V
### NOMINATION AND ELECTION OF DIRECTORS

Section 1. Nomination. Nomination for election to the Board of Directors shall be made by a Nominating Committee. Nominations may also be made from the floor at the annual meeting. The Nominating Committee shall consist of a Chairman, who shall be a member of the Board of Directors, and two (2) or more members of the Association. The Nominating Committee shall be appointed by the Board of Directors prior to each annual meeting of the members, to serve from the close of such annual meeting until the close of the next annual meeting; and such appointment shall be announced at each annual meeting. The Nominating Committee shall make as many nominations for election to the Board of Directors as it shall in its discretion determine, but not less than the number of vacancies that are to be filled. Such nominations may, be made from among members or non-members.


Section 2. Election. Election to the Board of Directors shall be by secret written ballot. At each election, the members or their proxies may cast, in respect to each vacancy, as many votes as they are entitled to exercise under the provisions of the Declaration. The persons receiving the largest number of votes shall be elected. Cumulative voting is not permitted.


## ARTICLE VI
### MEETINGS OF DIRECTORS

Section 1. Regular Meetings. Regular meetings of the Board of Directors shall be held monthly without notice at such place and hour as may be fixed from time to time by resolution of the Board. Should said meeting fall upon a legal holiday, then that meeting shall be held at the same time on the next day which is not a legal holiday.

Section 2. Special Meetings. Special meetings of the Board of Directors shall be held when called by the president of the Association, or by any two (2) Directors, after not less than three (3) days' notice to each Director.

Section 3. Quorum. A majority of the number of Directors shall constitute a quorum for the transaction of business. Every act or decision done or made by a majority of the Directors present at a duly held meeting at which a quorum is present shall be regarded as the act of the Board.


## ARTICLE VII
### POWERS AND DUTIES OF THE BOARD OF DIRECTORS
  
Section 1. Powers. The Board of Directors shall have power to:

(a) Adopt and publish rules and regulations governing the use of the Common Area and facilities, and the personal conduct of the members and their guests thereon, and to establish penalties for the infraction thereof;

(b) Suspend the voting rights and right to use of the recreational facilities of a member during any period in which such member shall be in default in the payment of any assessment levied by the Association. Such rights may also be suspended after notice and hearing for a period not to exceed sixty (60) days for infraction of published rules and regulations;

(c) Exercise for the Association all powers, duties and authority vested in or delegated to this Association and not reserved to the membership by other provisions of these Bylaws, the Articles of Incorporation or the Declaration;

(d) Declare the office of a member of the Board of Directors to be vacant in the event such member shall be absent from three (3) consecutive regular meetings of the Board of Directors; and

(e) Employ a manager, an independent contractor or such other employees as they deem necessary and to prescribe their duties.

Section 2. Duties. It shall be the duty of the Board of Directors to:

(a) cause to be kept a complete record of all its acts and corporate affairs and to present a statement thereof to the members at the annual meeting of the’ members, or at any special meeting when such statement is requested in writing by one-fourth (1/4) of the Class A members who are entitled to vote;

(b) Supervise all officers, agents and employees of this Association and to see that their duties are properly performed;

(c) As more fully provided in the Declaration, to:

(1) Fix the amount of the annual assessment against each Lot at least thirty (30) days in advance of each annual assessment period;

(2) Send written notice of each assessment to every owner subject thereto at least thirty (30) days in advance of each annual assessment period; and

(3) Foreclose the lien against any property for which assessments are not paid within thirty (30) days in advance of each annual assessment period; and

(d) Issue, or to cause an appropriate officer to issue, upon demand by any person, a certificate setting forth whether or not any assessment has been paid. A reasonable charge may be made by the Board for the issuance of these certificates. If a certificate states an assessment has been paid, such certificate shall be conclusive evidence of such payment;

(e) Procure and maintain adequate liability and hazard insurance on property owned by the Association;

(f) Cause all officers or employees having fiscal responsibilities to be bonded, as it may deem appropriate;

(g) Cause the Common Area to be maintained.

## ARTICLE VIII
### OFFICERS AND THEIR DUTIES

Section 1. Enumeration of Offices. The officers of this Association shall be a president and vice-president, who shall at all times be members of the Board of Directors, a secretary and a treasurer, and such other officers as the Board may from time to time by resolution create.

Section 2. Election of Officers. The election of officers shall take place at the first meeting of the Board of Directors following each annual meeting of the members.

Section 3. Term. The officers of this Association shall be elected annually by the Board; and each shall hold office for one (1) year unless he shall sooner resign, or shall be removed or otherwise disqualified to serve.

Section 4. Special Appointments. The Board may elect such other offices as the affairs of the Association may re- quire, each of whom shall hold office for such period, have such authority and perform such duties as the Board may, from time to time, determine.

Section 5. Resignation and Removal. Any officer may be removed from office with or without cause by the Board. Any officer may resign at any time giving written notice to the Board, the president or the secretary. Such resignation shall take effect on the date of receipt of such notice or at any later time specified therein; and, unless otherwise specified therein, the acceptance of such resignation shall not be necessary to make it effective.

Section 6. Vacancies. A vacancy in any office may be filled by appointment by the Board. The officer appointed to such vacancy shall serve for the remainder of the term of the officer he replaces,

Section 7. Multiple Offices. The offices of secretary and treasurer may be held by the same person. No person shall simultaneously hold more than one (1) of any of the other offices except in the case of special offices created pursuant to Section 4 of this Article.

Section 8. Duties. The duties of the officers are as follows:

#### President

(a) The president shall preside at all meetings of the Board of Directors; shall see that orders and resolutions of the Board are carried out; shall sign all leases, mortgages, deeds and other written instruments and shall co-sign all checks and promissory notes.

#### Vice-President

(b) The vice-president shall act in the place and stead of the president in the event of his absence, inability or refusal to act and shall exercise and discharge such other duties as may be required of him by the Board.

#### Secretary

(c) The secretary shall record the votes and keep the minutes of all meetings and proceedings of the Board and of the members; keep the corporate seal of the Association and affix it on all papers requiring said seal; service notice of meetings of the Board and of the members; keep appropriate current records showing the members of the Association together with their addresses; and shall perform such other duties as required by the Board.

#### Treasurer

(d) The treasurer shall receive and deposit in appropriate bank accounts all monies of the Association and shall disburse such funds as directed by resolution of the Board of Directors; shall sign all checks and promissory notes of the Association; keep proper.books of account; cause an annual audit of the Association books to be made by a public accountant at the completion of each fiscal year; and shall prepare an annual budget and a statement of income and expenditures to be presented to the membership at its regular annual meeting, and deliver a copy of each to the members.

## ARTICLE IX
### COMMITTEES

"The Association shall appoint an Architectural Control Committee, as provided in the Declaration, and a Nominating Committee, as provided in these Bylaws. In addition, the Board of Directors shall appoint other committees as deemed appropriate in carrying out its purpose.

## ARTICLE X
### BOOKS AND RECORDS

The books, records and pavers of the Association shall at all times, during reasonable business hours, be subject to inspection by any member. The Declaration, the Articles of Incorporation and the Bylaws of the Association shall be available for inspection by any member at the principal office of the Association where copies may be purchased at reasonable cost.

## ARTICLE XI
### ASSESSMENTS

As more fully provided in the Declaration, each member is obligated to pay to the Association annual and special assessments which are secured by a continuing lien upon the property against which the assessment is.made. Any assessments which are not paid when due shall be delinquent If the assessment is not paid within thirty (30) days after the due date, the assessment shall bear interest from the date of delinquency at the rate of six percent (6%) per annum; and the Association may bring an action at law against the owner personally obligated to pay the same or foreclose the lien against the property, and interest, costs and reasonable attorney's fees of ny such action shall be added to the amount of such assessment. No owner may waiver or otherwise escape liability for the assessments provided for herein by nonuse of the Common Area or abandonment of his Lot.

## ARTICLE XII
### CORPORATE SEAL

The Association shall have a seal in circular form having within its circumference the words: LA CUESTA ASSOCIATION, INC.

## ARTICLE XIII
### AMENDMENTS

Section 1. These Bylaws may be amended, at a regular or special meeting of the members, by a vote of a majority of a quorum of members present in person or by proxy, except that the Federal Housing Administration or the Veterans Administration shall have the right to veto amendments while there is a Class B membership.

Section 2. In the case of any conflict between the Articles of Incorporation and these Bylaws, the Articles shall control; and, in the case of any conflict between the Declaration and these Bylaws, the Declaration shall control.

## ARTICLE XIV
### MISCELLANEOUS

The fiscal year of the Association shall begin on the first day of January and end on the 31st day of December of every year, except that the first fiscal year shall begin on the date of incorporation. 

IN WITNESS WHEREOF, we, being all the Directors of the LA CUESTA ASSOCIATION, have hereunto set our hands this 9th day of March, 1978.

Jpseph Contadino
President
    
Larry C. Fisher
Vice-President

John W. Magura, Jr.
Secretary

Joseph A. Brichler, Jr.
Treasurer


STATE OF ARIZONA    )
                    ) ss:
County of Maricopa  )

The foregoing Bylaws of LA CUESTA ASSOCIATION were acknowledged before me this 9th day of March, 1978, by Joseph Contadino, John W. Magura, Jr., Larry C. Fischer and Joseph A. Brichler, Jr.

Notary Public
My commission expires:
9-26-80


## CERTIFICATION

I, the undersigned, do hereby certify:

THAT I am the duly elected and acting secretary of the LA CUESTA ASSOCIATION, an Arizona corporation; and

THAT the foregoing Bylaws constitute the original Bylaws of said Association, as duly adopted at a meeting of the Board of Directors thereof held on the 1st day of March, 1978.

IN WITNESS WHEREOF, I have hereunto subscribed my name and affixed the seal of said Association this 9th day of March, 1978.


# AMENDMENT TO BYLAWS LA CUESTA ASSOCIATION

At the annual meeting of the membership of La Cuesta Association, held March 15, 1984, the following amendments to the Association Bylaws were approved by a vote of a majority of a quorum of the Association members present in person or by proxy:

1. Article II, Sections 6 and 7, are deleted.
2. Article IV, Section 1, was amended to read as follows:

The affairs of the Association shall be managed by a Board of not less than three (3), nor more than twenty-five (25) Directors, who must be members of the Association.

3. Article V, Section 1, the last sentence thereof, was amended to read as follows:

Such nominations may be made from the members only.

IN WITNESS WHEREOF, we, being all the Directors of the LA CUESTA ASSOCIATION have hereunto set our hands this 3rd of April, 1984.
 

Bob Smith -- President-Director
Loney Stewart -- Vice President-Director

   
# AMENDMENT TO BYLAWS LA CUESTA ASSOCIATION

At the annual meeting of the membership of La Cuesta Association, held March 27", 2012, the following amendment to the Association Bylaws was approved by a vote of a majority of a quorum of the Association members present in person or by ballot:

Article VI, Section 1, was amended to read as follows:

Regular meetings of the Board of Directors shall be held bi-monthly beginning in January without notice at such place and hour as may be fixed from time to time by resolution of the Board. (January, March, May, July, September and November.)

WITNESS WHEREOF, we, being all the Directors of the LA CUESTA ASSOCIATION have hereunto set our signature this 22nd day of May, 2012.

Mickey Laurent - President
Frank Sales - Vice President
Jeanine Korer - Secretary
Bobbie Snow - Treasurer
Stevan Earl - Director
Barb Monie - Director
Myron Snow - Director
Ron Rossman - Director
Steve Tugenberg - Director
